package it.ads.esercizio9.model.dao;

import java.util.List;

import it.ads.esercizio9.model.entity.Article;
import it.ads.esercizio9.model.entity.Comment;

public interface ICommentDao {
	
	public List<Comment> listAllComments();
	
	void persistComment(Comment article);
	
	void saveOrUpdateComment(Comment article);
	
	void deleteComment(Comment article);
	
	Comment findCommentByIdComment(Integer idComment);
	
	List<Comment> findCommentsByArticle(Article article);
}
