package it.ads.esercizio9.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.transaction.annotation.Transactional;

import it.ads.esercizio9.model.dao.IArticleDao;
import it.ads.esercizio9.model.entity.Article;


public class ArticleDaoImpl implements IArticleDao {
	
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	public ArticleDaoImpl() {
	}

	@Transactional
	public List<Article> listAllArticles() {
		List<Article> outcome = entityManager.createQuery("select a from Article a", Article.class).getResultList();
		for (Article article : outcome ){
			System.out.println("Title:"+article.getTitle()+"Author:"+article.getAuthor()+"Body:"+article.getBody());
		}
		return outcome;
	 }
	
	public void saveOrUpdateArticle(Article article) {
	}
	
	public void persistArticle(Article article) {
	}

	public void deleteArticle(Article article) {
		entityManager.remove(article);
		
	}

	public Article findArticleByIdArticle(Integer idArticle) {
		return null;
	}

	

	


}
