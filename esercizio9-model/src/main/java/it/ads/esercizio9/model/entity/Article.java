package it.ads.esercizio9.model.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author daniele
 */
@Entity
@Table(name = "articles", catalog = "esercizio6")
public class Article implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer idArticle;
	private String title;
	private String author;
	private String body;
	private Date dateOfPost;
	private Set<Comment> comments;

	public Article() {
	}

	public Article(String title, String author, String body, Date dateOfPost) {
		this.title = title;
		this.author = author;
		this.body = body;
		this.dateOfPost = dateOfPost;
	}

	public Article(String title, String author, String body, Date dateOfPost, Set comments) {
		this.title = title;
		this.author = author;
		this.body = body;
		this.dateOfPost = dateOfPost;
		this.comments = comments;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idArticle", unique = true, nullable = false)
	public Integer getIdArticle() {
		return this.idArticle;
	}

	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}

	@Column(name = "title", nullable = false)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "author", nullable = false, length = 50)
	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Column(name = "body", nullable = false, length = 1000)
	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dateOfPost", nullable = false, length = 10)
	public Date getDateOfPost() {
		return this.dateOfPost;
	}

	public void setDateOfPost(Date dateOfPost) {
		this.dateOfPost = dateOfPost;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "article")
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Article [idArticle=" + idArticle + ", title=" + title + ", author=" + author + ", body=" + body
				+ ", dateOfPost=" + dateOfPost + ", comments=" + comments + "]";
	}

	
}
