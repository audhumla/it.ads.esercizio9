package it.ads.esercizio9.model.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

import it.ads.esercizio9.model.dao.ICommentDao;
import it.ads.esercizio9.model.entity.Article;
import it.ads.esercizio9.model.entity.Comment;

public class CommentDaoImpl implements ICommentDao {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	public List<Comment> listAllComments() {
		 return this.hibernateTemplate.loadAll(Comment.class);
	}

	public void persistComment(Comment comment) {
		
	}

	public void saveOrUpdateComment(Comment comment) {
		// TODO Auto-generated method stub
		
	}

	public void deleteComment(Comment comment) {
		this.hibernateTemplate.saveOrUpdate(comment);		
	}

	public Comment findCommentByIdComment(Integer idComment) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Comment> findCommentsByArticle(Article article) {
		// TODO Auto-generated method stub
		return null;
	}

}
