package it.ads.esercizio9.model.dao;

import java.util.List;

import it.ads.esercizio9.model.entity.Article;

public interface IArticleDao {
	
	public List<Article> listAllArticles();
	
	void persistArticle(Article article);
	
	void saveOrUpdateArticle(Article article);
	
	void deleteArticle(Article article);
	
	Article findArticleByIdArticle(Integer idArticle);

}
