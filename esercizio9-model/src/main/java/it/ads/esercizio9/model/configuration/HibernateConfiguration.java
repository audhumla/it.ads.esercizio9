/**
 * 
 */
package it.ads.esercizio9.model.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import it.ads.esercizio9.model.dao.IArticleDao;
import it.ads.esercizio9.model.dao.impl.ArticleDaoImpl;

/**
 * @author daniele
 *
 */
@Configuration
public class HibernateConfiguration {

	    @Value("${jdbc.driverClassName}")	private String driverClassName;
	    @Value("${jdbc.url}")               private String url;
	    @Value("${jdbc.username}")			private String username;
	    @Value("${jdbc.password}")          private String password;
	    @Value("${hibernate.dialect}")      private String hibernateDialect;
	    @Value("${hibernate.show_sql}")     private String hibernateShowSql;
	    @Value("${hibernate.hbm2ddl.auto}") private String hibernateHbm2ddlAuto;
	    
	    @Bean()    
	    public DataSource getDataSource()
	    {
	        DriverManagerDataSource ds = new DriverManagerDataSource();        
	        ds.setDriverClassName(driverClassName);
	        ds.setUrl(url);
	        ds.setUsername(username);
	        ds.setPassword(password);        
	        return ds;
	    }
	    
	    @Bean 		//Creating and registering in spring context an entityManager
	    @Autowired
	    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
	        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
	        em.setDataSource(getDataSource());
	        em.setPackagesToScan(new String[]{"it.ads.esercizio9.model"}); // package where are the @Entity classes are located, ususaly your domain package
	        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter(); // JPA implementation 
	        em.setJpaVendorAdapter(vendorAdapter);
	        return em;
	    }
	    
	    @Bean(name="articleDao")
	    public IArticleDao articleDao(){
	    	return new ArticleDaoImpl();
	    }
	

}
