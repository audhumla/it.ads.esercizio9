package it.ads.esercizio9.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *@author daniele
 */
@Entity
@Table(name = "comments", catalog = "esercizio6")
public class Comment implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer idComment;
	private Article article;
	private String title;
	private String author;
	private String body;
	private Date dateOfComment;

	public Comment() {
	}

	public Comment(String title, String author, String body, Date dateOfComment) {
		this.title = title;
		this.author = author;
		this.body = body;
		this.dateOfComment = dateOfComment;
	}

	public Comment(Article article, String title, String author, String body, Date dateOfComment) {
		this.article = article;
		this.title = title;
		this.author = author;
		this.body = body;
		this.dateOfComment = dateOfComment;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idComment", unique = true, nullable = false)
	public Integer getIdComment() {
		return this.idComment;
	}

	public void setIdComment(Integer idComment) {
		this.idComment = idComment;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idArticle")
	public Article getArticle() {
		return this.article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	@Column(name = "title", nullable = false)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "author", nullable = false, length = 50)
	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Column(name = "body", nullable = false, length = 1000)
	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dateOfComment", nullable = false, length = 10)
	public Date getDateOfComment() {
		return this.dateOfComment;
	}

	public void setDateOfComment(Date dateOfComment) {
		this.dateOfComment = dateOfComment;
	}

	@Override
	public String toString() {
		return "Comment [idComment=" + idComment + ", article=" + article + ", title=" + title + ", author=" + author
				+ ", body=" + body + ", dateOfComment=" + dateOfComment + "]";
	}

	
}
