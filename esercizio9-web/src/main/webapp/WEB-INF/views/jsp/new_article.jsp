<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="insert" value="/articles/insert_article"/>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Articles|Home page</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.grey-orange.min.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
<spring:url value="/resources/css/new_article.css" var="new_article"></spring:url>
<link rel="stylesheet" href="${new_article}" type="text/css">
</head>
<body>
	<div class="mdl-layout mdl-js-layout mdl-color--amber">
		<!-- <div class="mdl-layout mdl-js-layout">
			<header class="mdl-layout__header">
			</header>
		</div> -->
		<main class="mdl-layout__content">
			<div class="page-content">
				<div class="mdl-grid">
					<div class="mdl-card mdl-shadow--6dp mdl-cell--12-col">
						<div class="mdl-card__title mdl-color--primary mdl-color-text--white">
							<h2 class="mdl-card__title-text">Insert new Article</h2>
						</div>
					  	<div class="mdl-card__supporting-text">
							<form:form action="${insert}" class="insert" method="post" id="new_article">
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								        <label class="mdl-textfield__label">Title</label>
								        <input  name="title" type="text" class="mdl-textfield__input" id="input_title" required/>
								    </div>
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								        <label class="mdl-textfield__label">Author</label>
								        <input name="author" type="text" class="mdl-textfield__input" id="input_author" required>
								    </div>
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
								        <textarea name="body" class="mdl-textfield__input"  rows= "20" id="input_body" required></textarea>
								        <label class="mdl-textfield__label">Insert Body here...</label>
								    </div>
							</form:form>
						</div>
						<div class="mdl-card__actions mdl-card--border">
							<div class="mdl-layout-spacer"></div>
							<button type="submit" form="new_article" formaction="${insert}" value="Insert" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Insert</button>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
</body>
<script src="https://storage.googleapis.com/code.getmdl.io/1.1.3/material.min.js"></script>

</html>