<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="insert" value="/articles/${article.idArticle}"/>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.grey-orange.min.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
    <meta name="description" content="Articles printed by spring mvc">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>${article.title}</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <spring:url value="/resources/images/android-desktop.png" var="android-desktop"/>
    <link rel="icon" sizes="192x192" href="${android-desktop}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-orange.min.css">
    <spring:url value="/resources/css/articles.css" var="articlesCSS" />
	<spring:url value="/resources/js/articles.js" var="articlesJS" />
    <link rel="stylesheet" href="${articlesCSS}">
    <script type="text/javascript" src="${articlesJS}"></script>
</head>
 <body>
    <div class="demo-blog demo-blog--blogpost mdl-layout mdl-js-layout has-drawer is-upgraded">
      <main class="mdl-layout__content">
        <div class="demo-back">
        <spring:url value="/" var="home"></spring:url>
          <a class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" href="${home}" title="go back" role="button">
            <i class="material-icons" role="presentation">arrow_back</i>
          </a>
        </div>
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card mdl-shadow--4dp mdl-cell mdl-cell--12-col">
            <div class="mdl-card__media mdl-color-text--grey-50">
              <h3>${article.title}</h3>
            </div>
            <div class="mdl-color-text--grey-700 mdl-card__supporting-text meta">
              <div class="minilogo"></div>
              <div>
                <strong>${article.author}</strong>
                <span>${article.dateOfPost}</span>
              </div>
            </div>
            <div class="mdl-color-text--grey-700 mdl-card__supporting-text">
              <p>
              ${article.body}
              </p>
            </div>
            <div class="mdl-color-text--primary-contrast mdl-card__supporting-text comments">
              <!--  new comment here -->
              <form:form action="${insert}" method="post">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<label class="mdl-textfield__label">Title</label>
					<input  name="title" type="text" class="mdl-textfield__input" id="input_title" required/>
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<label class="mdl-textfield__label">Author</label>
					<input  name="author" type="text" class="mdl-textfield__input" id="input_title" required/>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl ">
                  	<textarea rows=1 class="mdl-textfield__input" id="comment" name="body"></textarea>
                  	<label for="comment" class="mdl-textfield__label">Join the discussion</label>
                  	<input name="idArticle" value="${article.idArticle}" type="hidden">
                </div>

               <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                	<i class="material-icons" role="presentation">check</i><span class="visuallyhidden">add comment</span>
               </button>
              </form:form>
           </div>
              <!-- Comments begin here -->
              	<c:forEach var="comment" items="${comments}">
	              <div class="comment mdl-color-text--grey-700">
	                <header class="comment__header">
	                  <spring:url value="/resources/images/co2.jpg" var="avatar"/>
	                  <img src="${avatar}" class="comment__avatar">
	                  <div class="comment__author">
	                    <strong>${comment.author}</strong>
	                    <span>${comment.dateOfComment}</span>
	                  </div>
	                </header>
	                <div class="comment__text">
	                	${comment.body}
	                </div>
	              </div>
	         	</c:forEach>
	       </div>
	         

          <nav class="demo-nav mdl-color-text--grey-50 mdl-cell mdl-cell--12-col">
   			<c:set var="newer" scope="session" value="${article.idArticle-1}"/>
   			<!-- TODO check if url gets no valid values -->
   			<spring:url var="urlNewer" value="/articles/${newer}"></spring:url>
            <a href="${urlNewer}" class="demo-nav__button">
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mdl-color--white mdl-color-text--grey-900" role="presentation">
                <i class="material-icons">arrow_back</i>
              </button>
              Older
            </a>
            <div class="section-spacer"></div>
   			<!-- TODO check if url gets no valid values -->
            <c:set var="older" scope="session" value="${article.idArticle+1}"/>
            <spring:url var="urlOlder" value="/articles/${older}"></spring:url>
            <a href="${urlOlder}" class="demo-nav__button">
              Newer
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mdl-color--white mdl-color-text--grey-900" role="presentation">
                <i class="material-icons">arrow_forward</i>
              </button>
            </a>
          </nav>
        </div>
      </main>
      <div class="mdl-layout__obfuscator"></div>
    </div>
</body>
</html>