<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.grey-orange.min.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
    <meta name="description" content="Articles printed by spring mvc">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Articles</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <spring:url value="/resources/images/android-desktop.png" var="android-desktop"/>
    <link rel="icon" sizes="192x192" href="${android-desktop}">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-orange.min.css">
    <spring:url value="/resources/css/articles.css" var="articlesCSS" />
	<spring:url value="/resources/js/articles.js" var="articlesJS" />
    <link rel="stylesheet" href="${articlesCSS}">
    <script type="text/javascript" src="${articlesJS}"></script>
</head>

<body>
    <div class="demo-blog mdl-layout mdl-js-layout has-drawer is-upgraded">
      <main class="mdl-layout__content">
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card coffee-pic mdl-cell mdl-cell--8-col">
            <div class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="#">Articles</a></h3>
            </div>
          </div>
          <div class="mdl-card something-else mdl-cell mdl-cell--8-col mdl-cell--4-col-desktop">
            <button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--fab mdl-color--accent">
              <i class="material-icons mdl-color-text--white" role="presentation">add</i>
              <span class="visuallyhidden">add</span>
            </button>
            <div class="mdl-card__media mdl-color--white mdl-color-text--grey-600">
            <spring:url value="/resources/images/logo.png" var="logo"/>
              <img src="${logo}">
            </div>
            <div class="mdl-card__supporting-text meta meta--fill mdl-color-text--grey-600">
              <div>
              </div>
              <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="menubtn">
              <spring:url value="/articles/new" var="new_article"/>
                <li class="mdl-menu__item"><a href="${new_article}">Insert new Article</a></li>
                <li class="mdl-menu__item">Link2</li>
                <li class="mdl-menu__item">Link3</li>
                <li class="mdl-menu__item">Link4</li>
              </ul>
              <button id="menubtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                <i class="material-icons" role="presentation">more_vert</i>
                <span class="visuallyhidden">show menu</span>
              </button>
            </div>
          </div>
          <c:forEach var="article" items="${articles}">
	          <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
	            <div class="mdl-card__media mdl-color-text--grey-50">
	              <spring:url value="/articles/${article.idArticle}" var="article_page"></spring:url>
	              <h3>
	              	<a href="${article_page}">${article.title}</a>
	             </h3>
	            </div>
	            <div class="mdl-color-text--grey-600 mdl-card__supporting-text">
	            	${article.body}
	            </div>
	          
	            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
	              <div class="minilogo"></div>
	              <div>
	                <strong>${article.author}</strong>
	                <span>${article.dateOfPost}</span>
	              </div>
	            </div>
	          </div>
	      </c:forEach>
	      
         
        </div>
      </main>
      <div class="mdl-layout__obfuscator"></div>
    </div>
    <script src="https://code.getmdl.io/1.1.3/material.min.js"></script>
  </body>
  <script>
    Array.prototype.forEach.call(document.querySelectorAll('.mdl-card__media'), function(el) {
      var link = el.querySelector('a');
      if(!link) {
        return;
      }
      var target = link.getAttribute('href');
      if(!target) {
        return;
      }
      el.addEventListener('click', function() {
        location.href = target;
      });
    });
  </script>
</html>

</body>
</html>