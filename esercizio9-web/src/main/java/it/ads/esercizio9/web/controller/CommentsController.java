package it.ads.esercizio9.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.ads.esercizio9.model.dao.ICommentDao;
import it.ads.esercizio9.model.dao.impl.CommentDaoImpl;
import it.ads.esercizio9.model.entity.Article;
import it.ads.esercizio9.model.entity.Comment;

@Controller
public class CommentsController {
	@RequestMapping(value = "/articles/{id}" , method=RequestMethod.POST)
	public String insertComment(@ModelAttribute("comment") Comment comment, @PathVariable("id") Integer id, BindingResult result, Model model){
		String ret = "/articles/"+id;
		if(result.hasErrors()) {
		       model.addAttribute("comment", comment);
		       return "articles/id";
		}
		//comment.setDateEqualsToday();
		System.out.println(id);
		//comment.setIdArticle(id);
		System.out.println("Insering new comment");
		System.out.println("Comment"+comment.toString());
		CommentDaoImpl commentDao = new CommentDaoImpl();
		//commentDao.insertComment(comment);
		model.addAttribute("comment", comment);
		return ret;
	}
		
	
	
}
